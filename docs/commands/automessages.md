# Messages Automatiques

Les messages automatiques sont une fonctionalité que d'autres bots pourraient également appeler `Annoncements`. Ce sont des messages qui sont prédéfinis et envoyés à interval régulier dans le salon de discussion. Ces messages peuvent être activés après un certain nombre de minutes depuis le dernier, un certain nombre de message postés dans le chat, ou les deux.

Les commandes de cette catégorie démarrent toutes avec `!am` ou sa version longue, `!automessage`.

----

## Ajouter un message

<span class='role moderator'>Modérateur</span>

Permet d'ajouter un message automatique à la liste de messages à envoyer.

```bash
!am add (votre message)
```

* **(votre message)** (_obligatoire_)
    * Le message que vous souhaitez ajouter à la liste de messages envoyés automatiquement.

----

## Changer le type de déclencheur

<span class='role moderator'>Modérateur</span>

Permet de changer la condition pour laquelle les messages automatiques seront envoyés dans le salon de discussion.

```bash
!am mode nombre
```

* **nombre** (_obligatoire_)
    * Le nombre correspondant au profil désiré pour les messages automatiques.
    * `1` Envoyer les messages basé sur les minutes uniquement. Dans ce mode, le message sera envoyé toutes les X minutes.
    * `2` Envoyer les messages basé sur l'activité du salon de discussion. tous les X messages postés (ceux du bot étant exclus), un message automatique sera envoyé.
    * `3` Envoyer les messages basé à la fois sur les minutes et l'activité du salon de discussion. Les messages seront envoyés toutes les X minutes, à condition que X messages ai été postés dans le salon de discussion.

----

## (dés)activer les messages

<span class='role moderator'>Modérateur</span>

Permet d'activer ou désactiver les messages de bienvenue. Cela conserve vos messages définis, mais permet de mettre en pause ou de reprendre l'envoi de ces messages par le bot.

```bash
# pour activer (au choix parmi ceux-ci)
!am 1
!am on
!an enable
# pour désactiver (au choix parmi ceux-ci)
!am 0
!am off
!am disable
```

* Les options `1`, `on` et `enable` sont toutes équivalentes et permettent d'activer les messages automatiques.
* Les options `0`, `off` et `disable` sont toutes équivalentes et permettent de désactiver les messages automatiques.

----

## Définir le nombre de messages

<span class='role moderator'>Modérateur</span>

Permet de définir après combien de messages postés dans le salon de discussion le prochain message automatique sera posté. Ce nombre de messages ne prends pas en compte les messages envoyés par le bot en lui-même.

```bash
!am set:messages nombre
```

* **nombre** (_obligatoire_)
    * Le nombre de messages requis entre deux envois de messages automatiques.

----

## Définir le nombre de minutes

<span class='role moderator'>Modérateur</span>

Permet de définir après combien de minutes doivent s'écouler entre le précédent message automatique pour pouvoir en poster un de nouveau.

```bash
!am set:minutes nombre
```

* **nombre** (_obligatoire_)
    * Le nombre de minutes requises entre deux envois de messages automatiques.

----

## Chercher l'ID d'un message

<span class='role moderator'>Modérateur</span>

Permet de trouver l'identifiant d'un message automatique basé sur des mots clés contenus dans le-dit message.

```bash
!am search (mots clés)
```

* **(mots clés)** (_obligatoire_)
    * une liste de termes, un mot, une phrase ou toute chose permettant d'identifier le message que vous cherchez

----

## Supprimer un message

<span class='role moderator'>Modérateur</span>

Permet de supprimer un message automatique via son ID. les messages suivant celui supprimé prendront sa place. Par exemple, supprimer le message avec l'ID 3 fera passer les messages ayant l'ID 4 et 5 vers l'ID 3 et 4.

```bash
!am del ID
!am remove ID
!am rm ID
!am delete ID
```

* **ID** (_obligatoire_)
    * L'identifiant du message automatique à supprimer.

----

## Mettre à jour un message

<span class='role moderator'>Modérateur</span>

Permet de mettre à jour un message automatique existant, en remplaçant l'ancien message par le nouveau message spécifié.

```bash
!am edit ID (nouveau message)
!am up ID (nouveau message)
!am update ID (nouveau message)
```

* **ID** (_obligatoire_)
    * L'identifiant du message automatique à remplacer
* **(nouveau message)** (_obligatoire_)
    * Le message qui doit remplacer celui précédemment défini pour l'identifiant.

----

## Voir un message

<span class='role moderator'>Modérateur</span>

Permet de voir le contenu d'un message automatique basé sur son ID.

```bash
!am info ID
!am show ID
!am see ID
!am view ID
```

* **ID** (_obligatoire_)
    * L'identifiant du message automatique à voir
