# Moyennes

Les moyennes sont un moyen simple et rapide de faire intéragir vos chatteurs afin qu'ils puissent donner une note sur quelque chose. Cela peut être par exemple une note sur la qualité d'une action faite en jeu, ou permettre au public de voter lors de concours. Les notes peuvent aller de 0 à 10 inclues. Basé sur le nombre de personnes ayant participées, la moyenne sera calculé lorsque la moyenne est fermée.

----

## Démarrer une moyenne

<span class='role moderator'>Modérateur</span>

Permet de lancer une calcule de moyenne.

Une fois cette commande lancée, Les personnes présentes dans le chat pourront donner une note de 0 à 10 incluse, et si plusieurs notes sont données par une seule personne seule la plus récente sera prise en compte.

```bash
!avg on
```

----

## Démarrer une moyenne

<span class='role moderator'>Modérateur</span>

Permet de terminer une calcule de moyenne.

Une fois cette commande lancée, Les notes enregistrées seront utilisées pour calculer la moyenne de celles-ci, puis retournées en réponse à cette commande.

```bash
!avg off
```