# Commandes personnalisées

Les commandes personnalisées sont le cœur même de tout bot sur Twitch. Que ce soit pour facilement afficher son message de raid, promouvoir vos réseaux sociaux, ou toute autre action répétitive, les commandes personnalisées sont un très bon moyen de gérer les choses récurrentes et régulièrement expliquées.

----

## Créer une commande

<span class='role moderator'>Modérateur</span>

Permet de créer une commande personnalisée sur votre chaîne. Les commandes seront toujours préfixées par un point d'exclamation (`!`) et retourneront un texte prédéfini.

```bash
!cmd add nom (votre message)
```

* **nom** (_obligatoire_)
    * Le nom de la commande qui sera créée. Celle-ci doit être en une seule partie. Par exemple, si l'on utilise `test` comme nom, la commande sera `!test`. Le point d'exclamation est ajouté automatiquement.
* **(votre message)** (_obligatoire_)
    * Le message à renvoyer lorsque la commande est appelée dans le salon de discussion. Notez que toutes les commandes créées sont accessibles à toutes et tous (spectateurs, vips, modérateurs, Broadcaster, ...).

----

## Modifier une commande

<span class='role moderator'>Modérateur</span>

Modifier une commande est intégré de base dans la logique de [`!cmd add`](#creer-une-commande). Si la commande existe déjà, celle-ci sera mise à jour pour afficher le message spécifié dans la commande présentement tapée.

Toutefois, des alias ont été créés afin de vous aider à mettre à jour une commande sans devoir penser à taper `add` de nouveau.

```bash
!cmd edit nom (votre message)
!cmd update nom (votre message)
!cmd set nom (votre message)
```

Pour une description des arguments, veuillez vous référer à [Créer une commande](#creer-une-commande).

----

## Supprimer une commande

<span class='role moderator'>Modérateur</span>

Permet de supprimer une commande personnalisée sur votre chaîne.

```bash
!cmd delete nom
!cmd del nom
!cmd rm nom
```

* **nom** (_obligatoire_)
    * Le nom de la commande qui sera supprimée. Celle-ci doit être en une seule partie. Le point d'exclamation est optionnel et sera ajouté si non présent.
