# Autres commandes

Les commandes dans cette catégorie ne sont pas groupable avec d'autres commandes similaires.

----

## !fc

<span class='role viewer'>Spectateur</span>

Permet de savoir depuis combien de temps vous suivez la chaîne dans laquelle vous tapez la commande.

Retourne la date du dernier suivi de la chaîne (basé sur les API de Twitch) ainsi que la durée approximative en années, mois, jours, heures et minutes. Le calcul ne prends pas en compte le calendrier et considère que chaque mois possède 30 jours.

```bash
!fc
```

----

## !uptime

<span class='role viewer'>Spectateur</span>

Permet de savoir depuis combien de temps le stream a été démarré.

```bash
!uptime
```

----

## !mihoka

<span class='role viewer'>Spectateur</span>

Affiche les informations concernant le bot, incluant la version, son objectif en tant que bot, ainsi qu'un lien vers cette documentation.

```bash
!mihoka
```

----

## !roll

<span class='role viewer'>Spectateur</span>

Lance un dé entre 1 et  `max` (6 par défaut). Le maximum peut être changé en spécifiant le paramètre `max`.

```bash
!roll [max]
```

* **max** (_optionel_, valeur par défaut `6`)
  * Permet de définir le maximum (inclusif) auquel le dé peut générer un nombre. Le nombre doit être strictement positif si spécifié.

----

## !so / !shoutout

<span class='role moderator'>Modérateur</span>

Effectue la promotion d'un autre streameur sur Twitch, affichant l'adresse de sa page Twitch et invitant les membres du chat à aller suivre la personne.

```bash
!so username
!shoutout username
```

* **username**  (_obligatoire_)
  * Le nom de connexion de la personne que vous souhaitez promouvoir. Par exemple, si une personne possède le nom twitch `weasel` mais son nom d'affichage est `족제비` (la version coréenne de `Weasel`) il faut utiliser le nom twitch (la version n'utilisant que des lettres de l'alphabet latin basique).
