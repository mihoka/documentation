# Giveaways

Les giveaways sont un moyen de tirer au sort un gagnant parmis une liste de participants. Lorsqu'un giveaway est démarré, les spectateurs peuvent taper une commande afin de rejoindre celui-ci. Aucune variable ne modifiera les chances de gain pour les participants: que la personne soit simple spectateur, abonné ou modérateur, leur chance sera pour toutes et tous de 1 sur le nombre de participants.

----

## Démarrer un giveaway

<span class='role moderator'>Modérateur</span>

Permet de lancer un tirage au sort, activant la possibilité pour les spectateurs d'utiliser [`!raffle`](#participer).

```bash
!ga start
```

----

## Participer

<span class='role viewer'>Spectateur</span>

Permet de vous enregistrer comme participant dans un giveaway précédemment démarré via [`!ga start`](#demarrer-un-giveaway).

```bash
!raffle
```

----

## Voir les détails du giveaway

<span class='role moderator'>Modérateur</span>

Permet de voir diverses informations concernant le giveaway, telles que depuis quand celui-ci est démarré, combien de participants sont en lice pour le tirage au sort, etc.

```bash
!ga status
```

----

## Cloturer un giveaway

<span class='role moderator'>Modérateur</span>

Permet de mettre fin aux inscriptions pour le giveaway en cours. La cloture du giveaway est également implicitement réalisé lors du tirage au sort du ou des gagnant(s) via la commande [`!ga draw`](#tirer-un-gagnant).

----

## Tirer un gagnant

<span class='role moderator'>Modérateur</span>

Permet de tirer un ou plusieurs gagnant(s) pour le giveaway en cours. Cette commande peut être répétée jusqu'a ce que plus aucun participant ne soit dans la liste. Les participants tirés au sort via cette commande sont déplacés dans une liste de gagnants, empêchant ceux-ci d'être tirés de nouveau si la commande est répétée.

```bash
!ga draw [nombre]
```

* **nombre** (_optionel_)
    * Le nombre de gagnants à tirer au sort. Par défaut, **un seul gagnant sera tiré** si le nombre n'est pas spécifié.