# Messages de bienvenue

Les commandes de ce groupe permettent de configurer les messages automatiques lorsqu'une personne poste un premier message dans le salon de discussion après un certain délai écoulé sans écrire aucune intéraction.

L'ensemble des sous-commandes commencent avec `!welcome`, suivi d'un espace et du terme de la sous-commande, ainsi que d'éventuels arguments.

----

## mode

<span class='role moderator'>Modérateur</span>

Permet de changer le mode de bienvenue pour le bot. Cela permet également, via le mode `0`, de désactiver le message de bienvenue automatique.

```bash
!welcome mode nombre
```

* **nombre** (_obligatoire_)
    * `0`: désactives les messages de bienvenue pour tous le monde.
    * `1`: active les messages de bienvenue pour n'importe-qu'elle personne postant un message dans le salon de discussion
    * `2`: active les messages de bienvenue pour les personnes ayant été ajoutées aux membres VIP et/ou ayant un message personnalisé uniquement.

----

## message (générique)

<span class='role moderator'>Modérateur</span>

Permet de changer le message utilisé pour souhaiter la bienvenue aux personnes discutant pour la première fois depuis un certain délai, dans le salon de discussion.

```bash
!welcome message (votre message)
```

* **(votre message)** (_obligatoire_)
    * Le message de bienvenue à envoyer lorsque la personne poste son premier message dans le salon. vous pouvez utiliser `%s` afin de remplacer celui-ci par le nom de la personne en question.
    * Par exemple, `bienvenue %s!` affichera `bienvenue Lumikkode!` si `Lumikkode` poste son premier message dans le salon.

----

## message (personne spécifique)

<span class='role moderator'>Modérateur</span>

Permet de changer le message de bienvenue pour une personne spécifique dans le salon de discussion. Ce message spécifique à une personne aura la priorité face au message générique de bienvenue si défini.

```bash
!welcome messageFor username (votre message)
```

* **username** (_obligatoire_)
    * Le nom de connexion de la personne à qui vous souhaitez éditer le message de bienvenue. Par exemple, si une personne possède le nom twitch `weasel` mais son nom d'affichage est `족제비` (la version coréenne de `Weasel`) il faut utiliser le nom twitch (la version n'utilisant que des lettres de l'alphabet latin basique).
* **(votre message)** (_obligatoire_)
    * Le message de bienvenue à envoyer lorsque la personne poste son premier message dans le salon. vous pouvez utiliser `%s` afin de remplacer celui-ci par le nom de la personne en question.
    * Par exemple, `bienvenue %s!` affichera `bienvenue Lumikkode!` si `Lumikkode` poste son premier message dans le salon.

----

## Supprimer (spécifique)

<span class='role moderator'>Modérateur</span>

Permet de retirer le message de bienvenue configuré pour une personne spécifique. Seul les messages spécifiques peuvent êtres supprimés car les messages génériques peuvent simplement être désactivés en utilisant la sous-commande [!welcome mode](#mode).

```bash
!welcome deleteFor username
```

* **username** (_obligatoire_)
    * Le nom de connexion de la personne à qui vous souhaitez éditer le message de bienvenue. Par exemple, si une personne possède le nom twitch `weasel` mais son nom d'affichage est `족제비` (la version coréenne de `Weasel`) il faut utiliser le nom twitch (la version n'utilisant que des lettres de l'alphabet latin basique).

----

## Délai

<span class='role moderator'>Modérateur</span>

Permet de définir le délai (en minutes) avant lequel une personne est considérée comme absente (aucun message posté durant x minutes), et recevra donc un message de bienvenue après avoir posté de nouveau un message dans le salon de discussion.

```bash
!welcome delay minutes
```

* **minutes** (_obligatoire_)
    * La durée (en minutes) avant laquelle une personne ne soit considérée absente (si aucun message n'a été posté de sa part).
