# À propos de Mihoka

**Mihoka** est un bot Twitch entièrement gratuit, open-source et hébergé par son créateur (rien à installer, donc) qui a pour but de faciliter la vie  de tous les jours des streameurs. Contrairement aux autres bots tels que `StreamElements`, `StreamLabs` ou encore `NightBot`, **Mihoka** se veut être plus "humaine" dans son comportement. De plus, son créateur est enclin à réaliser des développements personalisés pour les streameurs ayant un besoin très spécifique mais sans connaissances en programmation. Les demandes considérées assez intéressantes pour les autres sont également ajoutées de manière globale au bot.

Pour obtenir de l'aide ou avoir le bot sur votre channel, n'hésitez pas à vous rendre sur [le discord du bot](https://discord.gg/WfG4gU9CQj).
